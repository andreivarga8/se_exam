public class Main2 {
    public static void main(String[] args) {
        IThread IThread1 = new IThread("IThread1");
        IThread IThread2 = new IThread("IThread2");

        Thread thread1 = new Thread(IThread1);
        Thread thread2 = new Thread(IThread2);

        thread1.start();
        thread2.start();
    }
}

class IThread implements Runnable {
    private String name;
    private int message_no;

    public IThread(String name) {
        this.name = name;
        this.message_no = 1;
    }


    @Override
    public void run() {

        while (this.message_no <= 10) {

            System.out.println("[" + this.name + "]" + " --> [" + this.message_no + "]");
            try {
                Thread.sleep(4000);
                this.message_no++;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}