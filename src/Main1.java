public class Main1 {
    public static void main(String[] args) {

    }
}

class A {

    public A() {

    }
}

class B extends A {
    private long t;

    public B() {

    }

    public void b() {

    }

    public void processC(C c) {

    }
}

class C {

    public C() {

    }
}

class D {

    private E e;
    private F f;
    private B b;

    //
    public D(F f) {
        this.e = new E();
        this.f = f;
    }

    public void setB(B b) {
        this.b = b;
    }

    public void met1(int i) {

    }
}


class E {

    public E() {

    }

    public void met2(){

    }
}

class F {

    public F() {

    }
}
